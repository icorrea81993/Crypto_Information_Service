package com.AccumulationCryptoWebService.AccumulationCryptoWebService;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AccumulationCryptoWebServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(AccumulationCryptoWebServiceApplication.class, args);
	}

}
