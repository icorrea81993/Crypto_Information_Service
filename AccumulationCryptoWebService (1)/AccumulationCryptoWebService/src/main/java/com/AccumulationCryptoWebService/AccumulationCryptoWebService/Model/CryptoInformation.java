package com.AccumulationCryptoWebService.AccumulationCryptoWebService.Model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.Objects;

@Entity
public class CryptoInformation {

    @Id
    @GeneratedValue
    public int id;

    public String assetName;
    public String assetTicker;

    public long twentyFourHourVolume;
    public long marketCap;

    public CryptoInformation(int id, String assetName, String assetTicker, long twentyFourHourVolume, long marketCap) {
        this.id = id;
        this.assetName = assetName;
        this.assetTicker = assetTicker;
        this.twentyFourHourVolume = twentyFourHourVolume;
        this.marketCap = marketCap;
    }

    public CryptoInformation() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAssetName() {
        return assetName;
    }

    public void setAssetName(String assetName) {
        this.assetName = assetName;
    }

    public String getAssetTicker() {
        return assetTicker;
    }

    public void setAssetTicker(String assetTicker) {
        this.assetTicker = assetTicker;
    }

    public long getTwentyFourHourVolume() {
        return twentyFourHourVolume;
    }

    public void setTwentyFourHourVolume(long twentyFourHourVolume) {
        this.twentyFourHourVolume = twentyFourHourVolume;
    }

    public long getMarketCap() {
        return marketCap;
    }

    public void setMarketCap(long marketCap) {
        this.marketCap = marketCap;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CryptoInformation)) return false;
        CryptoInformation that = (CryptoInformation) o;
        return getId() == that.getId() &&
                getTwentyFourHourVolume() == that.getTwentyFourHourVolume() &&
                getMarketCap() == that.getMarketCap() &&
                Objects.equals(getAssetName(), that.getAssetName()) &&
                Objects.equals(getAssetTicker(), that.getAssetTicker());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getAssetName(), getAssetTicker(), getTwentyFourHourVolume(), getMarketCap());
    }

    @Override
    public String toString() {
        return "CryptoInformation{" +
                "id=" + id +
                ", assetName='" + assetName + '\'' +
                ", assetTicker='" + assetTicker + '\'' +
                ", twentyFourHourVolume=" + twentyFourHourVolume +
                ", marketCap=" + marketCap +
                '}';
    }
}
