package com.AccumulationCryptoWebService.AccumulationCryptoWebService.Repository;

import com.AccumulationCryptoWebService.AccumulationCryptoWebService.Model.CryptoInformation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CryptoInformationRepository extends JpaRepository<CryptoInformation, Integer> {
}
