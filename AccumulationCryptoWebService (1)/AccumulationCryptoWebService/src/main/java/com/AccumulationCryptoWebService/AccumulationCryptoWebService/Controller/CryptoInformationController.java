package com.AccumulationCryptoWebService.AccumulationCryptoWebService.Controller;

import com.AccumulationCryptoWebService.AccumulationCryptoWebService.Model.CryptoInformation;
import com.AccumulationCryptoWebService.AccumulationCryptoWebService.Repository.CryptoInformationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

@RestController
public class CryptoInformationController {


    @Autowired
    public CryptoInformationRepository cryptoRepo;

    @GetMapping("/cryptoInformation")
    public List<CryptoInformation> getAllStockInfo() {
        return cryptoRepo.findAll();
    }

    @GetMapping("/cryptoInformation/{id}")
    public Optional<CryptoInformation> retrieveAsset(@PathVariable int id){
        return cryptoRepo.findById(id);
    }

    @DeleteMapping("/cryptoInformation/{id}")
    public void deleteAsset(@PathVariable int id) {
        cryptoRepo.deleteById(id);
    }
}
